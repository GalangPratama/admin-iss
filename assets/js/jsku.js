//Datatables Delivered ISS
$(document).ready(function () {

  $(function() {
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#filterdelivered span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }


      $('#filterdelivered').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
      }, cb);

      cb(start, end);

    });

   $('#filterdelivered').on('apply.daterangepicker', function(ev, picker) {
   var start = picker.startDate;
   var end = picker.endDate;

    $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
      var min = start;
      var max = end;
      var startDate = new Date(data[1]);
      
      if (min == null && max == null) {
        return true;
      }
      if (min == null && startDate <= max) {
        return true;
      }
      if (max == null && startDate >= min) {
        return true;
      }
      if (startDate <= max && startDate >= min) {
        return true;
      }
      return false;
    }
  );
  dataku.draw();
  $.fn.dataTable.ext.search.pop();
  });


 var dataku = $('#data_delivered').DataTable({
      "oLanguage": {
        "sSearch": "Search" //Will appear on search form
      },
      dom: 'lrtip',
      dom: 'Bfrtip',
      buttons: [
        {
          extend: 'excelHtml5',
          text: 'PRINT EXCEL',
          title: 'Laporan POD ISS Indonesia',
          autoFilter: true,
          exportOptions: {
            columns:  [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
          }               
        }
        ],
        columnDefs:[
          { className: "dt-head-center", "targets": "_all" },
          { className: "dt-body-center", "targets": "_all" },
          {  
           "targets":[5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 ], 
           "visible": false  
          },
        ],
     "ajax":{  
      url:"../laporan/fetch_iss_delivered",  
    },  
    columns: [
    { "data": null },  
    { "data": null,
      render:function(data, type, row)
      {
        return moment(data['trnDate']).format('DD MMM YYYY');
      }
    },
    { "data": "trnNoHAWB" },
    { "data": "trnDeliveredByName" },
    { "data": null,
      render:function(data, type, row)
      {
        return moment(data['trnDeliveredDate']).format('DD MMM YYYY');
      }
    },
    { "data": "trnDeliveredTime" },
    { "data": "trnSpecialInstruction" },
    { "data": "trnConsName" },
    { "data": "trnConsAlm1" },
    { "data": "trnPhone" },
    { "data": "trnConsContact" },
    { "data": "trnNote" },
    { "data": "trnkoli" },
    { "data": "trnWeight" },
    { "data": "trnReceiver" },
    { "data": "trnReceiverRelation" },
    { "data": "trnNoHAWB" },
    { "data": null, defaultContent: '' },
    { "data": "trnReceivernotes" },
    { "data": null, defaultContent: '' },
    { "data": null,
    render:function(data, type, row)
    {
      return '<button type="button" name="modal_image" id="'+ data['trnNoHAWB'] +'" class="modal_image btn btn-warning btn-xs"><i class="md-pageview">Detail</i></button>';
    }
  }
  ],
  
  });

  dataku.on( 'order.dt search.dt', function () {
    dataku.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
  } ).draw();

  // $('#table-filter').on('change', function(){
  //   dataku.search(this.value).draw();   
  // });


  $('#filterdelivered').on('apply.daterangepicker', function(ev, picker) {
   var start = picker.startDate;
   var end = picker.endDate;

    $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
      var min = start;
      var max = end;
      var startDate = new Date(data[4]);
      
      if (min == null && max == null) {
        return true;
      }
      if (min == null && startDate <= max) {
        return true;
      }
      if (max == null && startDate >= min) {
        return true;
      }
      if (startDate <= max && startDate >= min) {
        return true;
      }
      return false;
    }
  );
  dataku.draw();
  $.fn.dataTable.ext.search.pop();
  });
});
// End Datatables

//Datatables Return ISS
$(document).ready(function () {

  $(function() {
    var start = moment().subtract(29, 'days');
    var end = moment();
  
    function cb(start, end) {
        $('#filterreturn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }
  
  
      $('#filterreturn').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
      }, cb);
  
      cb(start, end);
  
    });
  
     $('#filterreturn').on('apply.daterangepicker', function(ev, picker) {
     var start = picker.startDate;
     var end = picker.endDate;
  
      $.fn.dataTable.ext.search.push(
      function(settings, data, dataIndex) {
        var min = start;
        var max = end;
        var startDate = new Date(data[1]);
        
        if (min == null && max == null) {
          return true;
        }
        if (min == null && startDate <= max) {
          return true;
        }
        if (max == null && startDate >= min) {
          return true;
        }
        if (startDate <= max && startDate >= min) {
          return true;
        }
        return false;
      }
    );
    dataku.draw();
    $.fn.dataTable.ext.search.pop();
  });
  
  
   var dataku = $('#data_return').DataTable({
        "oLanguage": {
          "sSearch": "Search" //Will appear on search form
        },
        dom: 'lrtip',
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'excelHtml5',
            text: 'PRINT EXCEL',
            title: 'Laporan Return ISS Indonesia',
            autoFilter: true,
            exportOptions: {
              columns:  [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
            }               
          }
          ],
          columnDefs:[
            { className: "dt-head-center", "targets": "_all" },
            { className: "dt-body-center", "targets": "_all" },
            {  
             "targets":[5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19], 
             "visible": false  
            },
          ],
       "ajax":{  
        url:"../laporan/fetch_iss_return",  
      },  
      columns: [
      { "data": null },  
      { "data": null,
        render:function(data, type, row)
        {
          return moment(data['trnDate']).format('DD MMM YYYY');
        }
      },
      { "data": "trnNoHAWB" },
      { "data": "trnDeliveredByName" },
      { "data": null,
        render:function(data, type, row)
        {
          return moment(data['trnDeliveredDate']).format('DD MMM YYYY');
        }
      },
      { "data": "trnDeliveredTime" },
      { "data": "trnSpecialInstruction" },
      { "data": "trnConsName" },
      { "data": "trnConsAlm1" },
      { "data": "trnPhone" },
      { "data": "trnConsContact" },
      { "data": "trnNote" },
      { "data": "trnkoli" },
      { "data": "trnWeight" },
      { "data": "trnReceiver" },
      { "data": "trnReceiverRelation" },
      { "data": "trnNoHAWB" },
      { "data": null, defaultContent: '' },
      { "data": "trnReceivernotes" },
      { "data": null, defaultContent: '' },
      { "data": null,
      render:function(data, type, row)
      {
        return '<button type="button" name="modal_image" id="'+ data['trnNoHAWB'] +'" class="modal_image btn btn-warning btn-xs"><i class="md-pageview">Detail</i></button>';
      }
    }
    ],
    
  });
  
    dataku.on( 'order.dt search.dt', function () {
      dataku.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
      } );
    } ).draw();
  
    // $('#table-filter').on('change', function(){
    //   dataku.search(this.value).draw();   
    // });
  
  
    $('#filterreturn').on('apply.daterangepicker', function(ev, picker) {
     var start = picker.startDate;
     var end = picker.endDate;
  
      $.fn.dataTable.ext.search.push(
      function(settings, data, dataIndex) {
        var min = start;
        var max = end;
        var startDate = new Date(data[4]);
        
        if (min == null && max == null) {
          return true;
        }
        if (min == null && startDate <= max) {
          return true;
        }
        if (max == null && startDate >= min) {
          return true;
        }
        if (startDate <= max && startDate >= min) {
          return true;
        }
        return false;
      }
    );
    dataku.draw();
    $.fn.dataTable.ext.search.pop();
  });
});
// End Datatables 




//Datatables Delivered BMW
$(document).ready(function () {

  $(function() {
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#filterdeliveredbmw span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }


      $('#filterdeliveredbmw').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
      }, cb);

      cb(start, end);

    });

 var data_delivered_bmw = $('#data_delivered_bmw').DataTable({
      "oLanguage": {
        "sSearch": "Search" //Will appear on search form
      },
      dom: 'lrtip',
      dom: 'Bfrtip',
      buttons: [
        {
          extend: 'excelHtml5',
          text: 'PRINT EXCEL',
          title: 'Laporan POD BMW Indonesia',
          autoFilter: true,
          exportOptions: {
            columns:  [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
          }               
        }
        ],
        columnDefs:[
          { className: "dt-head-center", "targets": "_all" },
          { className: "dt-body-center", "targets": "_all" },
          {  
           "targets":[5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 ], 
           "visible": false  
          },
        ],
     "ajax":{  
      url:"../laporan/fetch_bmw_delivered",  
    },  
    columns: [
    { "data": null },  
    { "data": null,
      render:function(data, type, row)
      {
        return moment(data['trnDate']).format('DD MMM YYYY');
      }
    },
    { "data": "trnNoHAWB" },
    { "data": "trnDeliveredByName" },
    { "data": null,
      render:function(data, type, row)
      {
        return moment(data['trnDeliveredDate']).format('DD MMM YYYY');
      }
    },
    { "data": "trnDeliveredTime" },
    { "data": "trnSpecialInstruction" },
    { "data": "trnConsName" },
    { "data": "trnConsAlm1" },
    { "data": "trnPhone" },
    { "data": "trnConsContact" },
    { "data": "trnNote" },
    { "data": "trnkoli" },
    { "data": "trnWeight" },
    { "data": "trnReceiver" },
    { "data": "trnReceiverRelation" },
    { "data": "trnNoHAWB" },
    { "data": null, defaultContent: '' },
    { "data": "trnReceivernotes" },
    { "data": null, defaultContent: '' },
    { "data": null,
    render:function(data, type, row)
    {
      return '<button type="button" name="modal_image" id="'+ data['trnNoHAWB'] +'" class="modal_image btn btn-warning btn-xs"><i class="md-pageview">Detail</i></button>';
    }
  }
  ],
  
  });

  data_delivered_bmw.on( 'order.dt search.dt', function () {
    data_delivered_bmw.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
  } ).draw();

  $('#filterdeliveredbmw').on('apply.daterangepicker', function(ev, picker) {
   var start = picker.startDate;
   var end = picker.endDate;

    $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
      var min = start;
      var max = end;
      var startDate = new Date(data[4]);
      
      if (min == null && max == null) {
        return true;
      }
      if (min == null && startDate <= max) {
        return true;
      }
      if (max == null && startDate >= min) {
        return true;
      }
      if (startDate <= max && startDate >= min) {
        return true;
      }
      return false;
    }
  );
  data_delivered_bmw.draw();
  $.fn.dataTable.ext.search.pop();
  });
});
// End Datatables

//Datatables Return BMW
$(document).ready(function () {

  $(function() {
    var start = moment().subtract(29, 'days');
    var end = moment();
  
    function cb(start, end) {
        $('#filterreturnbmw span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }
  
  
      $('#filterreturnbmw').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
      }, cb);
  
      cb(start, end);
  
    });
  
   var data_return_bmw = $('#data_return_bmw').DataTable({
        "oLanguage": {
          "sSearch": "Search" //Will appear on search form
        },
        dom: 'lrtip',
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'excelHtml5',
            text: 'PRINT EXCEL',
            title: 'Laporan Return BMW Indonesia',
            autoFilter: true,
            exportOptions: {
              columns:  [1, 2, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
            }               
          }
          ],
          columnDefs:[
            { className: "dt-head-center", "targets": "_all" },
            { className: "dt-body-center", "targets": "_all" },
            {  
             "targets":[5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19], 
             "visible": false  
            },
          ],
       "ajax":{  
        url:"../laporan/fetch_bmw_return",  
      },  
      columns: [
      { "data": null },  
      { "data": null,
        render:function(data, type, row)
        {
          return moment(data['trnDate']).format('DD MMM YYYY');
        }
      },
      { "data": "trnNoHAWB" },
      { "data": "trnDeliveredByName" },
      { "data": null,
        render:function(data, type, row)
        {
          return moment(data['trnDeliveredDate']).format('DD MMM YYYY');
        }
      },
      { "data": "trnDeliveredTime" },
      { "data": "trnSpecialInstruction" },
      { "data": "trnConsName" },
      { "data": "trnConsAlm1" },
      { "data": "trnPhone" },
      { "data": "trnConsContact" },
      { "data": "trnNote" },
      { "data": "trnkoli" },
      { "data": "trnWeight" },
      { "data": "trnReceiver" },
      { "data": "trnReceiverRelation" },
      { "data": "trnNoHAWB" },
      { "data": null, defaultContent: '' },
      { "data": "trnReceivernotes" },
      { "data": null, defaultContent: '' },
      { "data": null,
      render:function(data, type, row)
      {
        return '<button type="button" name="modal_image" id="'+ data['trnNoHAWB'] +'" class="modal_image btn btn-warning btn-xs"><i class="md-pageview">Detail</i></button>';
      }
    }
    ],
    
  });
  
    data_return_bmw.on( 'order.dt search.dt', function () {
      data_return_bmw.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
      } );
    } ).draw();
  
  
    $('#filterreturnbmw').on('apply.daterangepicker', function(ev, picker) {
     var start = picker.startDate;
     var end = picker.endDate;
  
      $.fn.dataTable.ext.search.push(
      function(settings, data, dataIndex) {
        var min = start;
        var max = end;
        var startDate = new Date(data[4]);
        
        if (min == null && max == null) {
          return true;
        }
        if (min == null && startDate <= max) {
          return true;
        }
        if (max == null && startDate >= min) {
          return true;
        }
        if (startDate <= max && startDate >= min) {
          return true;
        }
        return false;
      }
    );
    data_return_bmw.draw();
    $.fn.dataTable.ext.search.pop();
  });
});
// End Datatables 





//Modal Detail
$(document).on('click', '.modal_image', function(){  
    var user_id = $(this).attr("id");  
      $.ajax({  
           url:"../laporan/fetch_single_user",  
           method:"POST",
           data:{user_id:user_id},  
           dataType:"json",  
           success:function(data)  
           {  
                $('#detailModal').modal('show');  
                $('#trnDeliveredByName').text(data.trnDeliveredByName);
                $('#trnNoPlat').text(data.trnNoPlat);
                $('#trnDeliveredDate').text(data.trnDeliveredDate);
                $('#trnDeliveredTimePOD').text(data.trnDeliveredTime);
                $('#trnAcc').text(data.trnAcc);
                $('#trnName').text(data.trnName);
                $('#trnSpecialInstruction').text(data.trnSpecialInstruction);
                $('#trnConsName').text(data.trnConsName);
                $('#trnConsAlm1').text(data.trnConsAlm1);
                $('#trnPhone').text(data.trnPhone);
                $('#trnConsContact').text(data.trnConsContact);
                $('#trnkoli').text(data.trnkoli);
                $('#trnWeight').text(data.trnWeight);
                $('#trnReceiver').text(data.trnReceiver);
                $('#trnReceiverRelation').text(data.trnReceiverRelation); 
                $('#trnReceivernotes').text(data.trnReceivernotes);
                $('#jdimg1').html(data.Image1);
                $('.modal-header #trnNoHAWB').text(user_id);
                $('.modal-header #trnDate').text(data.trnDate); 
                $('.modal-header #trnTime').text(data.trnTime);  
           } 
        });
});
//End Modal Detail    
      
$(document).ready(function () {

  $(function() {
    var start = moment().subtract(29, 'days');
    var end = moment();
  
    function cb(start, end) {
        $('#filterindex span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }
  
  
      $('#filterindex').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
      }, cb);
  
      cb(start, end);
  
    });
  
     $('#filterindex').on('apply.daterangepicker', function(ev, picker) {
     var start = picker.startDate;
     var end = picker.endDate;
  
      $.fn.dataTable.ext.search.push(
      function(settings, data, dataIndex) {
        var min = start;
        var max = end;
        var startDate = new Date(data[10]);
        
        if (min == null && max == null) {
          return true;
        }
        if (min == null && startDate <= max) {
          return true;
        }
        if (max == null && startDate >= min) {
          return true;
        }
        if (startDate <= max && startDate >= min) {
          return true;
        }
        return false;
      }
    );
    dataku.draw();
    $.fn.dataTable.ext.search.pop();
  });
  
  
   var dataku = $('#data_index').DataTable({
        "oLanguage": {
          "sSearch": "Filter Data" //Will appear on search form
        },
        "columnDefs": [ {
          "searchable": false,
          "orderable": false,
          "targets": 0
        } ],
        dom: 'lrtip',
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'excelHtml5',
            text: 'PRINT EXCEL',
            autoFilter: true,
            exportOptions: {
              columns:  [10, 1, 2, 3, 4, 5, 6, 7, 8, 9],
              stripHtml: false
            }               
          }
          ],
          columnDefs:[
            {  
             "targets":[ 4, 5, 7, 9 ], 
             "visible": false  
            },
          ],
       "ajax":{  
        url:"laporan/fetch_user_index",  
      },  
      columns: [
      { "data": null },  
      { "data": "trnHawb" },
      { "data": "trnDeliveredBy" },
      { "data": "trnNoPlat" },
      { "data": "trnCstAcc" },
      { "data": "trnCstName" },
      { "data": "trnReceiver" },
      { "data": "trnRelation" },
      { "data": "trnStatus" },
      { "data": "trnNote" },
      // ----- Image in Datatable ------
      // { data: "astPhoto",
      //   render: function (data, type, row, meta) {
      //       return '<img class="img-responsive" src="data:image/jpeg;base64,' + data +'"  height="100px" width="100px">';
      //   }
      // },
      { "data": null,
      render:function(data, type, row)
      {
        return moment(data['trnCreated']).format('DD MMM YYYY');
      }
      },
      { "data": null,
      render:function(data, type, row)
      {
        return '<button type="button" name="modal_index" id="'+ data['trnHawb'] +'" class="modal_index btn btn-warning btn-xs"><i class="md-pageview">Detail</i></button>';
      }
    }
    ],
    
  });
  
    dataku.on( 'order.dt search.dt', function () {
      dataku.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
      } );
    } ).draw();
  
    $('#table-filter').on('change', function(){
      dataku.search(this.value).draw();   
    });
  
  
    $('#filterindex').on('apply.daterangepicker', function(ev, picker) {
     var start = picker.startDate;
     var end = picker.endDate;
  
      $.fn.dataTable.ext.search.push(
      function(settings, data, dataIndex) {
        var min = start;
        var max = end;
        var startDate = new Date(data[10]);
        
        if (min == null && max == null) {
          return true;
        }
        if (min == null && startDate <= max) {
          return true;
        }
        if (max == null && startDate >= min) {
          return true;
        }
        if (startDate <= max && startDate >= min) {
          return true;
        }
        return false;
      }
    );
    dataku.draw();
    $.fn.dataTable.ext.search.pop();
  });
});
   
$(document).on('click', '.modal_index', function(){  
           var kurier_id = $(this).attr("id");  
             $.ajax({  
                  url:"laporan/fetch_single_index",  
                  method:"POST",
                  data:{kurier_id:kurier_id},  
                  dataType:"json",  
                  success:function(data)  
                  {  
                       $('#tesModal').modal('show');  
                       $('#idtrnHawb').text(data.trnHawb);
                       $('#trnDeliveredBy').text(data.trnDeliveredBy);
                       $('#trnNoPlat').text(data.trnNoPlat);
                       $('#trnCstAcc').text(data.trnCstAcc);
                       $('#trnCstName').text(data.trnCstName);
                       $('#trnReceiver').text(data.trnReceiver);
                       $('#trnRelation').text(data.trnRelation);
                       $('#trnNote').text(data.trnNote);
                       $('#trnStatus').text(data.trnStatus);
                       $('#jdimg1').html(data.Image1);
                       $('#jdimg2').html(data.Image2);
                       $('#jdimg3').html(data.Image3);
                       $('#jdimg4').html(data.Image4);
                       $('.modal-title #trnHawb').text(kurier_id);
                       $('.modal-title #trnCreated').text(data.trnCreated);  
                  } 
               });
});  
  
function toggleSidebar(){
    document.getElementById ("left-sidebar").classList.toggle('active');
}
function parseDateValue(rawDate) {

        var d = moment(rawDate, "DD-MMM-YYYY").format('DD-MM-YYYY');
        var dateArray = d.split("-");
        var parsedDate = dateArray[2] + dateArray[1] + dateArray[0];
        return parsedDate;
}

$(".reportrange").on('apply.daterangepicker', function (ev, picker) {

        ev.preventDefault();



        //if blank date option was selected
        if ((picker.startDate.format('DD-MMM-YYYY') == "01-Jan-0001") && (picker.endDate.format('DD-MMM-YYYY')) == "01-Jan-0001") {
            $(this).val('Blank');


            val = "^$";

            dataTable.column(dataIdx)
               .search(val, true, false, true)
               .draw();

        }
        else {
            //set field value
            $(this).val(picker.startDate.format('DD-MMM-YYYY') + ' to ' + picker.endDate.format('DD-MMM-YYYY'));



            //run date filter
            startDate = picker.startDate.format('DD-MMM-YYYY');
            endDate = picker.endDate.format('DD-MMM-YYYY');

            var dateStart = parseDateValue(startDate);
            var dateEnd = parseDateValue(endDate);

            var filteredData = dataTable
                    .column(dataIdx)
                    .data()
                    .filter(function (value, index) {

                        var evalDate = value === "" ? 0 : parseDateValue(value);
                        if ((isNaN(dateStart) && isNaN(dateEnd)) || (evalDate >= dateStart && evalDate <= dateEnd)) {

                            return true;
                        }
                        return false;
                    });


            var val = "";
            for (var count = 0; count < filteredData.length; count++) {

                val += filteredData[count] + "|";
            }

            val = val.slice(0, -1);


            dataTable.column(dataIdx)
                  .search(val ? "^" + val + "$" : "^" + "-" + "$", true, false, true)
                  .draw();
        }


       

});


    $(".daterange").on('cancel.daterangepicker', function (ev, picker) {
        ev.preventDefault();
        $(this).val('');
        dataTable.column(dataIdx)
              .search("")
              .draw();
    });





 