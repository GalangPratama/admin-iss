<div class="sidebar" id="sidebar">
    <div class="sidebar-inner">
        <div class="nav account-user pull-right">
            <div class="nav-item" id="akun">
                <a href="#" class="dropdown-toggle nav-link user-link" data-toggle="dropdown">
                    <span class="user-img">
                        <img class="rounded-circle" src="<?php echo base_url('assets/img/logo1.png');?>" width="70"
                            alt="<?php echo $this->session->userdata('ses_nama');?>">
                    </span>
                    <br>
                    <span class="status online"></span>
                    <span>Hi, <?php echo $this->session->userdata('ses_nama');?></span>
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item"
                        href="<?php echo site_url('user/profil/'.$this->session->userdata('ses_id')); ?>">My Profile</a>
                    <a class="dropdown-item" href="#">Edit Profile</a>
                    <a class="dropdown-item" href="#">Settings</a>
                    <a class="dropdown-item" href="<?php echo site_url('dashboard/logout'); ?>">Logout</a>
                </div>
            </div>
        </div>
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li
                    <?= $this->uri->segment(1) == 'dashboard' || $this->uri->segment(1) == '' ? 'class="active"' : '' ?>>
                    <a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-dashboard" aria-hidden="true"></i>
                        Dashboard </a>
                </li>
                <li
                    <?= $this->uri->segment(1) == 'statistik' || $this->uri->segment(1) == '' ? 'class="active"' : '' ?>>
                    <a href="<?php echo site_url('statistik'); ?>"><i class="fa fa-tasks" aria-hidden="true"></i>
                        Statistik </a>
                </li>
                <li
                    class="dropdown dropright <?= $this->uri->segment(2) == 'iss_delivered' || $this->uri->segment(2) == 'iss_return' ? 'active' : '' ?>">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fa fa-edit"
                            aria-hidden="true"></i>Reports ISS</a>
                    <ul class="dropdown-menu">
                        <li <?= $this->uri->segment(1) == 'iss_delivered' ? 'class="active"' : '' ?>><a
                                class="nav-link sub-menu" href="<?php echo site_url('laporan/iss_delivered'); ?>"><i
                                    class="fa fa-check-square-o" aria-hidden="true"></i> POD/Closed</a></li>
                        <li <?= $this->uri->segment(1) == 'iss_return' ? 'class="active"' : '' ?>><a
                                class="nav-link sub-menu" href="<?php echo site_url('laporan/iss_return'); ?>"><i
                                    class="fa fa-retweet" aria-hidden="true"></i> Return </a></li>
                    </ul>
                </li>
                <li
                    class="dropdown dropright <?= $this->uri->segment(2) == 'bmw_delivered' || $this->uri->segment(2) == 'bmw_return' ? 'active' : '' ?>">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fa fa-edit"
                            aria-hidden="true"></i>Reports BMW</a>
                    <ul class="dropdown-menu">
                        <li <?= $this->uri->segment(1) == 'bmw_delivered' ? 'class="active"' : '' ?>><a
                                class="nav-link sub-menu" href="<?php echo site_url('laporan/bmw_delivered'); ?>"><i
                                    class="fa fa-check-square-o" aria-hidden="true"></i> POD/Closed</a></li>
                        <li <?= $this->uri->segment(1) == 'bmw_return' ? 'class="active"' : '' ?>><a
                                class="nav-link sub-menu" href="<?php echo site_url('laporan/bmw_return'); ?>"><i
                                    class="fa fa-retweet" aria-hidden="true"></i> Return </a></li>
                    </ul>
                </li>
                <li>
                    <a href="http://ds.totallogisticsid.com/track/" target="_black"><i class="fa fa-search"
                            aria-hidden="true"></i> Track Package </a>
                </li>
                <li
                    class="dropdown dropright <?= $this->uri->segment(1) == 'user' || $this->uri->segment(1) == 'hapuslaporan' ? 'active' : '' ?>">
                    <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fa fa-database"
                            aria-hidden="true"></i>Master</a>
                    <ul class="dropdown-menu">
                        <li <?= $this->uri->segment(1) == 'user' ? 'class="active"' : '' ?>><a class="nav-link sub-menu"
                                href="<?php echo site_url('user'); ?>"><i class="fa fa-user-circle"
                                    aria-hidden="true"></i>Account User</a></li>
                        <li <?= $this->uri->segment(1) == '' ? 'class="active"' : '' ?>><a class="nav-link sub-menu"
                                href="#"><i class="fa fa-eraser" aria-hidden="true"></i>
                                Hapus Laporan </a></li>
                    </ul>
                </li>
                <li <?= $this->uri->segment(1) == 'logout' || $this->uri->segment(1) == '' ? 'class="active"' : '' ?>>
                    <a href="<?php echo site_url('dashboard/logout'); ?>"><i class="fa fa-sign-out"
                            aria-hidden="true"></i>
                        Logout </a>
                </li>
            </ul>
        </div>
    </div>
</div>