<div class="section-header">
    <h3 style="margin: 10px 0; text-align: center;"><?php echo $title; ?></h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Dashboard</li>
            <li class="breadcrumb-item" aria-current="page">Reports</li>
            <li class="breadcrumb-item active" aria-current="page">POD/Closed</li>
        </ol>
    </nav>
</div>
<div class="container">
    <div class="row" style="positin: absolute;">
        <div class="ftr-date col-4">
            <h5> Filter Tanggal : </h5>
            <div id="filterdeliveredbmw"
                style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                <i class="fa fa-calendar"></i>&nbsp;
                <span></span> <i class="fa fa-caret-down"></i>
            </div>
        </div>

        <!-- <div class="ftr-area col-4">
          <h5> Filter Status : </h5>
          <select id="table-filter" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
            <option value="">All</option>
            <option value="Delivered">Delivered</option>
            <option value="Return">Return</option>
          </select>
        </div> -->
    </div>
    <table id="data_delivered_bmw" class="table table-striped" style="width: 100%;">
        <thead>
            <tr>
                <th>No.</th>
                <th>Tanggal</th>
                <th>No. AWB</th>
                <th>Kurier Name</th>
                <th>Tanggal POD</th>
                <th>Jam POD</th>
                <th>JIT</th>
                <th>Area Name</th>
                <th>Alamat</th>
                <th>No. Telephone</th>
                <th>PIC</th>
                <th>NOTE</th>
                <th>Colly</th>
                <th>Weight</th>
                <th>Receipt Name</th>
                <th>NIK/Position Receipt</th>
                <th>No. AWB</th>
                <th>Remarks</th>
                <th>Status</th>
                <th>Pengembelian POD</th>
                <th>Detail</th>
            </tr>
        </thead>
    </table>
</div>