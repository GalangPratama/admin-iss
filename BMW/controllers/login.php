<?php
class Login extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->model('m_user');
		$this->cekLogin();
	}

	function index(){
		$data['title']="Login";
		$data['message']="";
		$this->load->view('login/index',$data);
	}
 
	function auth(){
		$user=$this->input->post('userid');
		$pass=$this->input->post('password');
 
        $cek_auth=$this->m_user->login($user,$pass);
 
        if($cek_auth->num_rows() > 0){ //jika login sebagai Admin
                $data=$cek_auth->row_array();
                $this->session->set_userdata('is_login',TRUE);
                 if($data['level']=='1'){ //Akses IT
                    $this->session->set_userdata('akses','1');
                    $this->session->set_userdata('ses_id',$data['username']);
					$this->session->set_userdata('ses_nama',$data['nama_lengkap']);
					$this->session->set_userdata('ses_foto',$data['foto']);
                    redirect('dashboard');
 
                 }else if($data['level']=='2'){ //akses Admin ISS
                    $this->session->set_userdata('akses','2');
                    $this->session->set_userdata('ses_id',$data['username']);
					$this->session->set_userdata('ses_nama',$data['nama_lengkap']);
					$this->session->set_userdata('ses_foto',$data['foto']);
					redirect('dashboard');
					
                 }else if($data['level']=='3'){ //akses Admin BMW
					$this->session->set_userdata('akses','3');
                    $this->session->set_userdata('ses_id',$data['username']);
					$this->session->set_userdata('ses_nama',$data['nama_lengkap']);
					$this->session->set_userdata('ses_foto',$data['foto']);
                    redirect('dashboard');
				 }else if($data['level']=='4'){ //akses User ISS
					$this->session->set_userdata('akses','4');
                    $this->session->set_userdata('ses_id',$data['username']);
					$this->session->set_userdata('ses_nama',$data['nama_lengkap']);
					$this->session->set_userdata('ses_foto',$data['foto']);
                    redirect('dashboard');
				 }else { // Akses User BMW
					$this->session->set_userdata('akses','5');
                    $this->session->set_userdata('ses_id',$data['username']);
					$this->session->set_userdata('ses_nama',$data['nama_lengkap']);
					$this->session->set_userdata('ses_foto',$data['foto']);
                    redirect('dashboard');
				 }
 
		}else{ // jika username dan password tidak ditemukan atau salah
			$data['title']="Login";
			$data['message']="<div class='alert alert-danger'>Username atau Password salah</div>";
			$this->load->view('login/index',$data);
        }
 
    }

	function cekLogin(){
		if($this->session->userdata('is_login')==true){
			redirect('dashboard');
		}
	}
}