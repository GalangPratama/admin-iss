<?php
class Laporan extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library(['template','pagination','form_validation']);
		$this->load->model(['m_laporan', 'Datakurier']);

		//---------------CSS-------------------
		$this->template->add_includes('css', 'assets/DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css');
		$this->template->add_includes('css', 'assets/DataTables/DataTables-1.10.18/css/jquery.dataTables.min.css');
		$this->template->add_includes('css', 'assets/DataTables/Buttons-1.5.6/css/buttons.dataTables.min.css');
		$this->template->add_includes('css', 'assets/DataTables/Responsive-2.2.2/css/responsive.bootstrap.min.css');
		$this->template->add_includes('css', 'assets/css/jquery-ui.css');
		$this->template->add_includes('css', 'assets/daterangepicker/daterangepicker.css');

		// ---------------Jquery------------- 
		$this->template->add_includes('js', 'assets/js/jsku.js');
		$this->template->add_includes('js', 'assets/DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js');
		$this->template->add_includes('js', 'assets/DataTables/DataTables-1.10.18/js/dataTables.bootstrap4.min.js');
		$this->template->add_includes('js', 'assets/DataTables/Buttons-1.5.6/js/dataTables.buttons.min.js');
		$this->template->add_includes('js', 'assets/DataTables/Buttons-1.5.6/js/buttons.html5.min.js');
		$this->template->add_includes('js', 'assets/DataTables/Buttons-1.5.6/js/buttons.print.min.js');
		$this->template->add_includes('js', 'assets/DataTables/Responsive-2.2.2/js/dataTables.responsive.min.js');
		$this->template->add_includes('js', 'assets/DataTables/Responsive-2.2.2/js/responsive.bootstrap.min.js');
		$this->template->add_includes('js', 'assets/DataTables/JSZip-2.5.0/jszip.min.js');
		$this->template->add_includes('js', 'assets/DataTables/buttons.colVis.min.js');
		$this->template->add_includes('js', 'assets/DataTables/vfs_fonts.js');
    	$this->template->add_includes('js', 'assets/DataTables/datetime.js');
		$this->template->add_includes('js', 'assets/js/jquery-ui.js');
		$this->template->add_includes('js', 'assets/daterangepicker/daterangepicker.js');
    	$this->template->add_includes('js', 'assets/daterangepicker/moment.min.js');

		if($this->session->userdata('is_login')==false){
			redirect('login');
		}
	}

	
	function index(){
		$data['title'] = "Reports Project";
		$this->template->load('template', 'laporan/index', $data);
	}

	function iss_delivered(){
		$data['title'] = "Reports Project POD";
		$this->template->load('template', 'laporan/iss_delivered', $data);
	}

	function iss_return(){
		$data['title'] = "Reports Project Return";
		$this->template->load('template', 'laporan/iss_return', $data);
	}

	function bmw_delivered(){
		$data['title'] = "Reports POD";
		$this->template->load('template', 'laporan/bmw_delivered', $data);
	}

	function bmw_return(){
		$data['title'] = "Reports Project Return";
		$this->template->load('template', 'laporan/bmw_return', $data);
	}

	function Tes(){
		$valid = $this->form_validation;
		$valid->set_error_delimiters('<i style="color: red;">', '</i>');
		$valid->set_rules('start_date', 'Field Start Date', 'required|trim|strip_tags|htmlspecialchars');
		$valid->set_rules('end_date', 'Field Start Date', 'required|trim|strip_tags|htmlspecialchars');
		
		if ($valid->run() === TRUE)
		{
			$input = $this->input->post(NULL, TRUE);
			$data = $this->m_laporan->filter_laporan($input["start_date"], $input["end_date"]);
			return $this->response([
				'data' => array_values($data)
			]);
		} else return  $this->response(['success' => FALSE, 'error' => validation_errors()]);
	}

	 function response($data)
    {
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit();
    }

    
	function cetak($id){
		//select lokasi from keluar_detail where id_trans='BL-20140604001' group by lokasi
		$data['title']="Laporan " .$id;
		$data['id']=$id;
		$data['job']=$this->m_laporan->cekDo($id)->result();
		$this->template->load('template','laporan/do',$data);
	}

		function hasil($id){
		$data['title']="Laporan " .$id;
		$data['id']=$id;
		$data['job']=$this->m_laporan->cekDo($id)->result();
    	$data['photo']=$this->m_laporan->photo($id)->result();
		$this->load->view('laporan/cetak_do',$data);
	}

// 	function fetch_user_index(){
// 		$fetch_data = $this->Datakurier->make_datatables_index(); 

// 		foreach($fetch_data as $row)  
// 		{    
// 			 $data = $fetch_data;          
// 		}
		 
// 		$output = array(
// 			 'data'                =>     $data,
// 			 "recordsTotal"        =>     $this->Datakurier->get_all_data_index(),  
// 		); 
// 		print_r(json_encode($output, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

//    }
	
	//LAPORAN ISS
	function fetch_iss_delivered(){
           $fetch_data = $this->Datakurier->make_iss_delivered(); 

           foreach($fetch_data as $row)  
           {    
                $data = $fetch_data;          
           }
            
           $output = array(
				'data'                =>     $data,
				"recordsTotal"        =>     $this->Datakurier->get_all_data(),  
           ); 
           print_r(json_encode($output, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
 
	}  
	  
	function fetch_iss_return(){
		$fetch_data = $this->Datakurier->make_iss_return(); 

		foreach($fetch_data as $row)  
		{    
			 $data = $fetch_data;          
		}
		 
		$output = array(
			 'data'                =>     $data,
			 "recordsTotal"        =>     $this->Datakurier->get_all_data(),  
		); 
		print_r(json_encode($output, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

	}
	
	//LAPORAN BMW
	function fetch_bmw_delivered(){
		$fetch_data = $this->Datakurier->make_bmw_delivered(); 

		foreach($fetch_data as $row)  
		{    
			 $data = $fetch_data;          
		}
		 
		$output = array(
			 'data'                =>     $data,
			 "recordsTotal"        =>     $this->Datakurier->get_all_data(),  
		); 
		print_r(json_encode($output, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

 	}  
   
 	function fetch_bmw_return(){
		$fetch_data = $this->Datakurier->make_bmw_return(); 

		foreach($fetch_data as $row)  
		{    
			$data = $fetch_data;          
		}
		
		$output = array(
			'data'                =>     $data,
			"recordsTotal"        =>     $this->Datakurier->get_all_data(),  
		); 
		print_r(json_encode($output, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));

	} 


	//DETAIL MODAL
    function fetch_single_user()  {  
           
           $output = array();
           $data = $this->Datakurier->fetch_single_user($_POST['user_id']);
           	foreach ($data as $row) {
				$output[] = $row;
			}
        		$output['trnNoHAWB']       = $row->trnNoHAWB;
				$output['trnDate'] = date('d M Y', strtotime($row->trnDate));
				$output['trnTime'] = date('H:i:s', strtotime($row->trnTime));
				$output['trnDeliveredByName'] = $row->trnDeliveredByName;
				$output['trnAcc'] = $row->trnAcc;
        		$output['trnName'] = $row->trnName;
				$output['trnConsName'] = $row->trnConsName;
        		$output['trnConsAlm1'] = $row->trnConsAlm1;
				$output['trnConsContact'] = $row->trnConsContact;
				$output['trnSpecialInstruction'] = $row->trnSpecialInstruction;
				$output['trnPhone'] = $row->trnPhone;
				$output['trnkoli'] = $row->trnkoli;
				$output['trnWeight'] = $row->trnWeight;
				$output['trnDeliveredDate'] = date('d M Y', strtotime($row->trnDeliveredDate));
				$output['trnDeliveredTime'] = date('H:i:s', strtotime($row->trnDeliveredTime));
				$output['trnReceiver'] = $row->trnReceiver;
				$output['trnReceivernotes'] = $row->trnReceivernotes;
				$output['trnNoPlat'] = $row->trnNoPlat;
				$output['trnReceiverRelation'] = $row->trnReceiverRelation;
                if($row->trnPhoto != '')  
                {  
					$output['Image1'] = ' <img width="250" height="250" src="http://202.138.229.86/api/pod/v2/uploads/'. $row->trnPhoto .'"/>';
                }  
                else  
                {  
                     $output['Image1'] = '<img width="200" height="200" src="'.base_url('assets/img/no-img.jpg').'" style="margin: auto;" />';
				}
			
           echo json_encode($output);  
	}
	  
	//   function fetch_single_index()  
    //   {  
           
	// 	$output = array();
	// 	$data = $this->Datakurier->fetch_single_index($_POST['kurier_id']);
	// 		foreach ($data as $row) {
	// 		 $output[] = $row;
	// 	 }
	// 		 $output['trnHawb']       = $row->trnHawb;
	// 		 $output['trnDeliveredBy'] = $row->trnDeliveredBy;
	// 		 $output['trnNoPlat'] = $row->trnNoPlat;
	// 		 $output['trnCstAcc'] = $row->trnCstAcc;
	// 		 $output['trnCstName'] = $row->trnCstName;
	// 		 $output['trnReceiver'] = $row->trnReceiver;
	// 		 $output['trnRelation'] = $row->trnRelation;
	// 		 $output['trnNote'] = $row->trnNote;
	// 		 $output['trnStatus'] = $row->trnStatus; 
	// 		 $output['trnCreated']  	= date('d M Y H:i:s', strtotime($row->trnCreated));   
	// 		 if($row->astSignature != '')  
	// 		 {  
	// 			 $output['Image1'] = ' <img width="250" height="200" src="data:image/jpeg;base64,' . $row->astSignature	 .'"/>';
	// 			 $output['Image2'] = ' <img width="250" height="200" src="data:image/jpeg;base64,' . $row->astPhoto	 .'"/>';
	 
	// 		 }  
	// 		 else  
	// 		 {  
	// 			  $output['Image1'] = '<img width="200" height="200" src="'.base_url('assets/img/no-img.jpg').'" style="margin: auto;" />';
	// 			  $output['Image2'] = '';
	// 			  $output['Image3'] = '';
	// 			  $output['Image4'] = '';  
	// 		 }
		 
	// 	echo json_encode($output);  
    //   }

      
}