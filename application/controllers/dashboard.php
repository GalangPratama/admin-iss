<?php
class Dashboard extends CI_Controller{
	var $folder   =   "maps";
	var $tables   =   "pod_trans";
	var $pk       =   "trnHawb";
	private $def_lat;
	private $def_lng;

	function __construct(){
		parent::__construct();
		$this->load->library(['template','pagination','form_validation','Googlemaps']);
		$this->load->model(['m_maps', 'm_user']);

		$this->template->add_includes('css', 'assets/css/Login.css');

		$this->template->add_includes('js', 'assets/js/googlemap.js');

		if($this->session->userdata('is_login')==false){
			redirect('login');
		}
 
		$this->def_lat=$this->config->item('default_lat');
		$this->def_lng=$this->config->item('default_lng');
	}

	
	function index(){
		if ($this->session->userdata('akses')=='3') {
			$warehouse =$this->m_maps->getAllBMW('pod_transdata')->result();
			//pagination
			//Set Lokasi pusat		
			$center='-6.248860, 106.953293';
			//Konfigurasi Gogle Map
			$cfg=array(
			'class'			=> 'map-canvas', //Class Div, disini bisa membuat map full size,dll
			'map_div_id'	=> 'map-canvas', //Definisi ID Default
			'center'		=> $center, //Set Lokasi pusat ke google map
			'zoom'			=> 'auto',	 // Besar map, katanya bagus yg 17 ini
			);
			

			$this->googlemaps->initialize($cfg);
			foreach ($warehouse as $row) {
				$marker = array();
				$marker['position'] = $row->trnLat.", ".$row->trnLong;
				$marker['infowindow_content'] = $row->trnDeliveredByName;
				$marker['icon'] = base_url().'/assets/img/marker_maps.png';
				$this->googlemaps->add_marker($marker);
				
			}
			
			//Buat HTML Map
			$data = array(
				'map'  => $this->googlemaps->create_map()
			);
			
				// Load the page
			$data['title']="Maps Kurir";
			$data['name']= $this->m_maps->getAllBMW('pod_trans')->result();
			$this->template->load('template','maps/index', $data);

		} else if($this->session->userdata('akses')=='2'){
			$warehouse =$this->m_maps->getAllISS('pod_transdata')->result();
			//pagination
			//Set Lokasi pusat		
			$center='-6.248860, 106.953293';
			//Konfigurasi Gogle Map
			$cfg=array(
			'class'			=> 'map-canvas', //Class Div, disini bisa membuat map full size,dll
			'map_div_id'	=> 'map-canvas', //Definisi ID Default
			'center'		=> $center, //Set Lokasi pusat ke google map
			'zoom'			=> 'auto',	 // Besar map, katanya bagus yg 17 ini
			);
			

			$this->googlemaps->initialize($cfg);
			foreach ($warehouse as $row) {
				$marker = array();
				$marker['position'] = $row->trnLat.", ".$row->trnLong;
				$marker['infowindow_content'] = $row->trnDeliveredByName;
				$marker['icon'] = base_url().'/assets/img/marker_maps.png';
				$this->googlemaps->add_marker($marker);
				
			}
			
			//Buat HTML Map
			$data = array(
				'map'  => $this->googlemaps->create_map()
			);
			
				// Load the page
			$data['title']="Maps Kurir";
			$data['name']= $this->m_maps->getAllISS('pod_trans')->result();
			$this->template->load('template','maps/index', $data);
		}else{
			$warehouse =$this->m_maps->getAll('pod_transdata')->result();
			//pagination
			//Set Lokasi pusat		
			$center='-6.248860, 106.953293';
			//Konfigurasi Gogle Map
			$cfg=array(
			'class'			=> 'map-canvas', //Class Div, disini bisa membuat map full size,dll
			'map_div_id'	=> 'map-canvas', //Definisi ID Default
			'center'		=> $center, //Set Lokasi pusat ke google map
			'zoom'			=> 'auto',	 // Besar map, katanya bagus yg 17 ini
			);
			

			$this->googlemaps->initialize($cfg);
			foreach ($warehouse as $row) {
				$marker = array();
				$marker['position'] = $row->trnLat.", ".$row->trnLong;
				$marker['infowindow_content'] = $row->trnDeliveredByName;
				$marker['icon'] = base_url().'/assets/img/marker_maps.png';
				$this->googlemaps->add_marker($marker);
				
			}
			
			//Buat HTML Map
			$data = array(
				'map'  => $this->googlemaps->create_map()
			);
			
				// Load the page
			$data['title']="Maps Kurir";
			$data['name']= $this->m_maps->getAll('pod_trans')->result();
			$this->template->load('template','maps/index', $data);
		}
	}

	function perkurir($id){
		$latlng = $this->m_maps->cekMap($id)->result();
		foreach ($latlng as $row ){
		$tes[] = $row->trnLat.", ".$row->trnLong;
		$center= $row->trnLat.", ".$row->trnLong;
		//Konfigurasi Gogle Map
		$cfg=array(
		'class'			=> 'map-kurir', //Class Div, disini bisa membuat map full size,dll
		'map_div_id'	=> 'map-kurir', //Definisi ID Default
		'center'		=> $center, //Set Lokasi pusat ke google map
		'zoom'			=> 'auto',	 // Besar map, katanya bagus yg 17 ini
		);
		
		$this->googlemaps->initialize($cfg);

			$marker = array();
			$marker['position'] = $row->trnLat.", ".$row->trnLong;
			$marker['infowindow_content'] = $row->trnDeliveredByName;
				if ($latlng[0]->trnLat.", ".$latlng[0]->trnLong) {
					$img_marker = base_url().'/assets/img/marker_maps.png';
				} else {
					$img_marker = base_url().'/assets/img/kurier.png';
				}
				
			$marker['icon'] = $img_marker;
			$this->googlemaps->add_marker($marker);

			$polyline['points'] = $tes;	
			$this->googlemaps->add_polyline($polyline);	
			}
		//Buat HTML Map
		$data = array(
			'map'  => $this->googlemaps->create_map()
		);
		
			// Load the page
		$data['title']="Maps Kurir";
		$data['name']= $this->m_maps->getAll('pod_transdata')->result();
		$data['detail']= $this->m_maps->cekMap($id)->row_array();
		$this->template->load('template','maps/index', $data);
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}
}
?>