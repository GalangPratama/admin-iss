<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jsonkurier extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Datakurier');
  }

   function fetch_user(){  
           $fetch_data = $this->Datakurier->make_datatables();  
           $data = array();  
           foreach($fetch_data as $row)  
           {  
                $sub_array = array();  
                $sub_array[] = $row->jdRef;  
                $sub_array[] = $row->jdNoDelevery;  
                $sub_array[] = $row->jdCreated;
                $sub_array[] = $row->jdConsignee;
                $sub_array[] = $row->jdReceiver;  
                $sub_array[] = $row->jdAddress;
                $sub_array[] = $row->jdNote;
                $sub_array[] = $row->jdStatus;
                $sub_array[] = '<button type="button" name="modal_image" id="'.$row->jdRef.'" class="modal_image btn btn-warning btn-xs">Image</button>';  
                $sub_array[] = '<a href="hasil/'. $row->jdRef.'" target="_blank" type="button" name="modal_pdf" class="modal_pdf btn btn-default btn-xs">PDF</a>';  
                $data[] = $sub_array;  
           }  
           $output = array(  
                "draw"                =>     intval($_POST["draw"]),  
                "recordsTotal"        =>     $this->Datakurier->get_all_data(),  
                "recordsFiltered"     =>     $this->Datakurier->get_filtered_data(),  
                "data"                =>     $data  
           );  
           echo json_encode($output);  
      }  

      function fetch_single_user()  
      {  
           $output = array();  
           $data = $this->Datakurier->fetch_single_user($_POST['user_id']);
             
           foreach($data as $row)
           {  
                $output['jobRef']        = $row->jobRef;  
                $output['jdConsignee']  = $row->jdConsignee;
                $output['jdStatus']  = $row->jdStatus;   
                if($row->apPhoto != '')  
                {  
                     $output['Image1'] = '<img width="200" src="data:image/jpeg;base64,'.$row->apPhoto.'"/>';
                     $output['Image2'] = '<img width="200" src="data:image/jpeg;base64,'.$row->apSignature.'"/>';
                      $output['Image3'] = '<img width="200" src="data:image/jpeg;base64,'.$row->apPhoto.'"/>';

                }  
                else  
                {  
                     $output['Image1'] = '<img width="200" src="'.base_url('assets/img/no-img.jpg').'"/>';
                     $output['Image2'] = '';
                     $output['Image3'] = '';  
                }  


                              
               
           }  
           echo json_encode($output);  
      }  

}