<?php
class Vehicle extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library(['template','pagination','form_validation']);
		$this->load->model('m_vehicle');
		$this->cekLogin();

		//---------------CSS-------------------
		$this->template->add_includes('css', 'assets/templates/css/style.css');
		$this->template->add_includes('css', 'assets/templates/css/components.css');

		// ---------------Jquery------------- 
		$this->template->add_includes('js', 'assets/templates/js/page/bootstrap-modal.js');

	}



	function index($offset=null){
		$limit=10;
		$data = array(
			'title' => "Data Kendaraan",
			'merk' => $this->m_vehicle->semua($limit,$offset)
		);

		//pagination
		$config['base_url']=site_url('vehicle/index');
		$config['total_rows']=$this->m_vehicle->jumlah();
		$config['per_page']=$limit;
		$this->pagination->initialize($config);
		$data['pagination']=$this->pagination->create_links();

		$this->template->load('template', 'vehicle/index', $data);
	}
	
	function tambah() {
		$type=$this->input->post('type');
        $plat=$this->input->post('plat');
        $status=$this->input->post('status');
        $this->m_vehicle->simpan($type,$plat,$status);
        redirect('vehicle');
	}

	function cekLogin(){
		$islogin=$this->session->userdata('is_login');
		$username=$this->session->userdata('username');
		$level=($this->session->userdata('level')=="Administrator") || ($this->session->userdata('level')=="operator");

		if($this->session->userdata('is_login')==false){
			redirect('login');
		}else if(!$level){
			redirect('template','home/tidakada');
		}
	}

	function validasi(){
		$this->form_validation->set_rules('type','Type Vahivle','required');
		$this->form_validation->set_rules('plat','Plat Nomer','required');
	}

	function hapus(){
		$kode=$this->input->post('kode');
		$this->m_vehicle->hapus($kode);
	}
      
}