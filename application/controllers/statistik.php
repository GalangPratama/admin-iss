<?php 
class statistik extends CI_Controller{
	var $folder   =   "maps";
	var $tables   =   "tb_jobdetail";
	var $pk       =   "jdRef";

	private $def_lat;
	private $def_lng;

	function __construct(){
		parent::__construct();
		$this->load->library(['template','pagination','form_validation']);
		$this->load->model(['m_home']);

		//---------------CSS-------------------
		// $this->template->add_includes('css', 'assets/templates/css/fullcalendar.min.css');
		// $this->template->add_includes('css', 'assets/templates/css/dataTables.bootstrap4.min.css');
		// $this->template->add_includes('css', 'assets/templates/css/select2.min.css');
		// $this->template->add_includes('css', 'assets/templates/css/bootstrap-datetimepicker.min.css');
		// $this->template->add_includes('css', 'assets/templates/plugins/morris/morris.css');
		// $this->template->add_includes('css', 'assets/templates/css/style.css');
		// ---------------Jquery------------- 
		
		$this->template->add_includes('js', 'assets/highchart/highcharts.js');
		$this->template->add_includes('js', 'assets/highchart/highcharts-3d.js');
		$this->template->add_includes('js', 'assets/highchart/modules/exporting.js');
		$this->template->add_includes('js', 'assets/highchart/modules/export-data.js');
		$this->template->add_includes('js', 'assets/js/custom_chart.js');

		
		if($this->session->userdata('is_login')==false){
			redirect('login');
		}

	}

	function index(){
		if ($this->session->userdata('akses')=='2') {
			$data['title']="Statistik Project ISS";
			$data['return'] = $this->m_home->get_rtn_iss();
			$data['deliv'] = $this->m_home->get_deliv_iss();
			$this->template->load('template', 'home/index', $data);
		} else if($this->session->userdata('akses')=='3') {
			$data['title']="Statistik Project BMW";
			$data['return'] = $this->m_home->get_rtn_bmw();
			$data['deliv'] = $this->m_home->get_deliv_bmw();
			$this->template->load('template', 'home/index', $data);
		}else if($this->session->userdata('akses')=='4') {
			$data['title']="Statistik Project ISS";
			$data['return'] = $this->m_home->get_rtn_iss();
			$data['deliv'] = $this->m_home->get_deliv_iss();
			$this->template->load('template', 'home/index', $data);
		}else if($this->session->userdata('akses')=='5') {
			$data['title']="Statistik Project BMW";
			$data['return'] = $this->m_home->get_rtn_bmw();
			$data['deliv'] = $this->m_home->get_deliv_bmw();
			$this->template->load('template', 'home/index', $data);
		}else{
			$data['title']="Statistik ALL Project";
			$data['returnbmw'] = $this->m_home->get_rtn_bmw();
			$data['delivbmw'] = $this->m_home->get_deliv_bmw();
			$data['returniss'] = $this->m_home->get_rtn_iss();
			$data['deliviss'] = $this->m_home->get_deliv_iss();
			$this->template->load('template', 'home/superadmin', $data);
		}
	}

	function get_pie(){
		
		$col1='DELIVERED';
		$col2='RETURN';
		$col3='OPEN';
		
		$cell1=$this->m_home->get_deliv();
		$cell2=$this->m_home->get_rtn();
		$cell3=$this->m_home->get_open();

		$row1 = array('name'=>$col1, 'y'=>$cell1);
		$row2 =array('name'=>$col2, 'y'=>$cell2);
		$row3  = array('name'=> $col3, 'y'=> $cell3);
		$data = array($row1, $row2, $row3);
		
		print_r(json_encode($data));
	}

	function get_line(){
		$data = $this->m_home->get_tgl();

		$category = array();
		$category['name'] = 'Tanggal';



		foreach ($data as $row)
		{
			$category['data'][] = $row->Tanggal;
		}

		$result = array();
		array_push($result,$category);


		print json_encode($result, JSON_NUMERIC_CHECK);

	}
	

	function setting(){
		$data['title']="Setting Identitas Perusahaan";
		$this->form_validation->set_rules('perusahaan','Nama Perusahaan','required');
		$this->form_validation->set_rules('pemilik','Nama Pemilik','required');
		$this->form_validation->set_rules('alamat','Alamat','required');

		if($this->form_validation->run()==true){

			$info=array(
				'nm_perusahaan'=>$this->input->post('perusahaan'),
				'pemilik'=>$this->input->post('pemilik'),
				'alamat'=>$this->input->post('alamat')
			);

			$cek=$this->m_identitas->identitas();
			if($cek->num_rows()>0){
				$this->m_identitas->update($info);
				$data['message']="<div class='alert alert-info'>Data Berhasil diupdate</div>";
			}else{
				$this->m_identitas->simpan($info);
				$data['message']="<div class='alert alert-info'>Data Berhasil disimpan</div>";
			}
		}else{
			$data['message']="";
		}
		$data['identitas']=$this->m_identitas->identitas()->row_array();
		$this->template->load('template','home/setting',$data);
	}

	function password(){
		$data['title']="Password";

		$this->form_validation->set_rules('lama','Password Lama','required');
		$this->form_validation->set_rules('baru','Password Baru','required');
		$this->form_validation->set_rules('konfirmasi','Konfirmasi Password','required|matches[baru]');

		if($this->form_validation->run()==true){
			$this->load->model('m_user');
			$username=$this->session->userdata('username');
			$lama=md5($this->input->post('lama'));

			$cek=$this->m_user->cekPassword($username,$lama);
			if($cek->num_rows()>0){
				$info=array(
					'password'=>md5($this->input->post('baru'))
				);
				$this->m_user->updatePassword($username,$info);
				$data['message']="<div class='alert alert-info'>Password Berhasil diubah</div>";
			}else{
				$data['message']="<div class='alert alert-danger'>Password Lama tidak sesuai</div>";
			}
		}else{
			$data['message']="";
		}
		$this->template->load('template','home/password',$data);
	}

	function profile(){
		$data['title']="User Profile";
		$data['profile']=$this->m_identitas->profile($this->session->userdata('username'))->row_array();
		$this->template->load('template','home/profile',$data);
	}

	function tidakada(){
		echo "Halaman tidak ditemukan";
	}


}