<?php
class Open extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library(['template','pagination','form_validation']);
		$this->load->model(['m_laporan', 'Datakurier']);

		//---------------CSS-------------------
		$this->template->add_includes('css', 'assets/DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css');
		$this->template->add_includes('css', 'assets/DataTables/DataTables-1.10.18/css/jquery.dataTables.min.css');
		$this->template->add_includes('css', 'assets/DataTables/Buttons-1.5.6/css/buttons.dataTables.min.css');
		$this->template->add_includes('css', 'assets/DataTables/Responsive-2.2.2/css/responsive.bootstrap.min.css');
		$this->template->add_includes('css', 'assets/css/jquery-ui.css');
		$this->template->add_includes('css', 'assets/daterangepicker/daterangepicker.css');

		// ---------------Jquery------------- 
		$this->template->add_includes('js', 'assets/js/jsku.js');
		$this->template->add_includes('js', 'assets/DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js');
		$this->template->add_includes('js', 'assets/DataTables/DataTables-1.10.18/js/dataTables.bootstrap4.min.js');
		$this->template->add_includes('js', 'assets/DataTables/Buttons-1.5.6/js/dataTables.buttons.min.js');
		$this->template->add_includes('js', 'assets/DataTables/Buttons-1.5.6/js/buttons.html5.min.js');
		$this->template->add_includes('js', 'assets/DataTables/Responsive-2.2.2/js/dataTables.responsive.min.js');
		$this->template->add_includes('js', 'assets/DataTables/Responsive-2.2.2/js/responsive.bootstrap.min.js');
		$this->template->add_includes('js', 'assets/DataTables/JSZip-2.5.0/jszip.min.js');
		$this->template->add_includes('js', 'assets/DataTables/buttons.colVis.min.js');
		$this->template->add_includes('js', 'assets/DataTables/vfs_fonts.js');
    	$this->template->add_includes('js', 'assets/DataTables/datetime.js');
		$this->template->add_includes('js', 'assets/js/jquery-ui.js');
		$this->template->add_includes('js', 'assets/daterangepicker/daterangepicker.js');
    	$this->template->add_includes('js', 'assets/daterangepicker/moment.min.js');

	}


	function index(){
		$data['title'] = "Order Open";
		$this->template->load('template', 'laporan/open', $data);
	}
	

	 function response($data)
    {
        $this->output
                ->set_status_header(200)
                ->set_content_type('application/json', 'utf-8')
                ->set_output(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                ->_display();
        exit();
    }

    
	function cetak($id){
		//select lokasi from keluar_detail where id_trans='BL-20140604001' group by lokasi
		$data['title']="Laporan " .$id;
		$data['id']=$id;
		$data['job']=$this->m_laporan->cekDo($id)->result();
		$this->template->load('template','laporan/do',$data);
	}

		function hasil($id){
		$data['title']="Laporan " .$id;
		$data['id']=$id;
		$data['job']=$this->m_laporan->cekDo($id)->result();
    $data['photo']=$this->m_laporan->photo($id)->result();
		$this->load->view('laporan/cetak_do',$data);
	}

	function cekLogin(){
		$islogin=$this->session->userdata('is_login');
		$username=$this->session->userdata('username');
		$level=($this->session->userdata('level')=="administrator") || ($this->session->userdata('level')=="operator");

		if($this->session->userdata('is_login')==false){
			redirect('login');
		}else if(!$level){
			redirect('template','home/tidakada');
		}
	}
			
	function fetch_user(){
           $fetch_data = $this->Datakurier->make_datatables(); 

           foreach($fetch_data as $row)  
           {    
                $data = $fetch_data;          
           }
            
           $output = array(
                'data'                =>     $data,  
           ); 
           print_r(json_encode($output, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
 
      }  

      function fetch_user_open(){
           $fetch_data = $this->Datakurier->make_datatables_open(); 
           foreach($fetch_data as $row)  
           {    
                $data = $fetch_data;          
           }
            
           $output = array(
                'data'                =>     $data,  
                "recordsTotal"        =>     $this->Datakurier->get_all_data(),  
                "recordsFiltered"     =>     $this->Datakurier->get_filtered_open(), 
           ); 
           print_r(json_encode($output, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
      }

      function fetch_single_user()  
      {  
           
           $output = array();
           $data = $this->Datakurier->fetch_single_user($_POST['user_id']);
                
        	foreach ($data as $row) {
        		$output[] = $row;
        	}
        		$output['jobRef']       = $row->jobRef;
        		$output['customerCode'] = $row->customerCode;
        		$output['customerName'] = $row->customerName;
        		$output['customerAddress'] = $row->customerAddress;
        		$output['jdNote'] = $row->jdNote; 
        		$output['jdGoodsType'] = $row->jdGoodsType;
            	$output['jdQty'] = $row->jdQty;
        		$output['jdReceiver'] = $row->jdReceiver; 
        		$output['jdReceiverTitle'] = $row->jdReceiverTitle; 
            	$output['jobCourier']  	= $row->jobCourier;
            	$output['jdStatus']  	= $row->jdStatus;
            	$output['jdUpdated']  	= date('d M Y H:i:s', strtotime($row->jdUpdated));   
                if($row->pod_photo != '')  
                {  
                	$output['Image1'] = ' <img width="250" height="200" src="data:image/jpeg;base64,' . $row->pod_signature	 .'"/>';
                	$output['Image2'] = ' <img width="250" height="200" src="http://202.138.229.86/api_woi/uploads/'. $output[0]->pod_photo .'"/>';
                	$output['Image3'] = ' <img width="250" height="200" src="http://202.138.229.86/api_woi/uploads/'. $output[1]->pod_photo .'"/>';
                	$output['Image4'] = ' <img width="250" height="200" src="http://202.138.229.86/api_woi/uploads/'. $output[2]->pod_photo .'"/>';
                }  
                else  
                {  
                     $output['Image1'] = '<img width="200" height="200" src="'.base_url('assets/img/no-img.jpg').'" style="margin: auto;" />';
                     $output['Image2'] = '';
                     $output['Image3'] = '';
                     $output['Image4'] = '';  
                }  
           echo json_encode($output);  
      }

      
}