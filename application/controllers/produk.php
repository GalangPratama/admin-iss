<?php
class Produk extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library(['template','pagination','form_validation']);
		$this->load->model('m_produk');
		$this->cekLogin();

		//---------------CSS-------------------
		$this->template->add_includes('css', 'assets/templates/css/style.css');
		$this->template->add_includes('css', 'assets/templates/css/components.css');

		// ---------------Jquery------------- 
		$this->template->add_includes('js', 'assets/templates/js/page/bootstrap-modal.js');

	}



	function index($offset=null){
		$limit=10;
		$data = array(
			'title' => "Item Barang",
			'merk' => $this->m_produk->semua($limit,$offset)
		);

		//pagination
		$config['base_url']=site_url('produk/index');
		$config['total_rows']=$this->m_produk->jumlah();
		$config['per_page']=$limit;
		$this->pagination->initialize($config);
		$data['pagination']=$this->pagination->create_links();

		$this->template->load('template', 'produk/index', $data);
	}
	
	function tambah() {
		$nama=$this->input->post('nama_produk');
        $status=$this->input->post('status');
        $this->m_produk->simpan($nama,$status);
        redirect('produk');
	}

	function cekLogin(){
		$islogin=$this->session->userdata('is_login');
		$username=$this->session->userdata('username');
		$level=($this->session->userdata('level')=="Administrator") || ($this->session->userdata('level')=="operator");

		if($this->session->userdata('is_login')==false){
			redirect('login');
		}else if(!$level){
			redirect('template','home/tidakada');
		}
	}

	function validasi(){
		$this->form_validation->set_rules('type','Type Vahivle','required');
		$this->form_validation->set_rules('plat','Plat Nomer','required');
	}

	function hapus(){
		$kode=$this->input->post('kode');
		$this->m_produk->hapus($kode);
	}
      
}