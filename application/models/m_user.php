<?php
class M_user extends CI_Model{
	private $table="iss_user";
	private $primary="username";

	function semua(){
		return $this->db->get($this->table);
	}

	function simpan($info){
		$this->db->insert($this->table,$info);
	}

	function cek($id){	
		$this->db->where($this->primary,$id);
		return $this->db->get($this->table);
	}

	function hapus($kode){
		$this->db->where($this->primary,$kode);
		$this->db->delete($this->table);
	}

	function update($user,$info){
		$this->db->where('username',$user);
		$this->db->update('iss_user',$info);
	}

	function cari($kode){
		$this->db->like("username",$kode);
		$this->db->or_like("nama_lengkap",$kode);
		return $this->db->get($this->table);
	}

	function login($user,$pass){
		$query = $this->db->query("SELECT * FROM iss_user WHERE username='$user' AND password=MD5('$pass') LIMIT 1");
		return $query;
	}

	function updatePassword($username,$info){
		$this->db->where('username',$username);
		$this->db->update('iss_user',$info);
	}

	function cekPassword($username,$lama){
		$this->db->where('username',$username);
		$this->db->where('password',$lama);
		return $this->db->get('iss_user');
	}
}