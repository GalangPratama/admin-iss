<?php
class M_home extends CI_Model{
	private $table="pod_trans";
	private $primary="trnHawb";
	

	function get_deliv_iss(){
		 $this->db->select("trnStatus");  
          $this->db->from('pod_tmp');
          $this->db->where('trnstatusId = "11"');
          $this->db->where('trnAcc = "3692000010"');
          return $this->db->count_all_results();
	}
	function get_rtn_iss(){
		 $this->db->select("trnStatus");  
     $this->db->from('pod_tmp');
     $this->db->where('trnstatusId <> "11"');
     $this->db->where('trnAcc = "3692000010"');
          return $this->db->count_all_results();
  }
  

  function get_deliv_bmw(){
    $this->db->select("trnStatus");  
         $this->db->from('pod_tmp');
         $this->db->where('trnstatusId = "11"');
         $this->db->where('trnAcc = "3691100167"');
         return $this->db->count_all_results();
 }
 function get_rtn_bmw(){
    $this->db->select("trnStatus");  
    $this->db->from('pod_tmp');
    $this->db->where('trnstatusId <> "11"');
    $this->db->where('trnAcc = "3691100167"');
         return $this->db->count_all_results();
 }


	function get_tgl(){
		$hasil = $this->db->query("SELECT date_format(trnCreated,'%d %M %Y') AS Tanggal FROM pod_trans WHERE trnCstAcc = '3692000010' GROUP BY date_format(trnCreated,'%Y %m %d')");
    $query= $hasil->result();
    return $query;
	}

  function get_line_deliv(){
    $hasil = $this->db->query("SELECT COUNT(IF(trnStatus='DELIVERED',1, NULL)) 'DELIVERED'
      FROM pod_trans
      WHERE trnCreated
      GROUP BY DATE_FORMAT(trnCreated,'%d %b %Y')");
    $result = $hasil->result_array();
        return $result;
  }

  function get_line_rtn(){
    $hasil = $this->db->query("SELECT COUNT(IF(trnStatus='RETURN',1, NULL)) 'RETURN'
      FROM pod_trans
      WHERE trnCreated
      GROUP BY DATE_FORMAT(trnCreated,'%d %b %Y')");
    $result = $hasil->result_array();
        return $result;
  }

  function get_line_open(){
    $hasil = $this->db->query("SELECT COUNT(IF(trnStatus='OPEN',1, NULL)) 'OPEN'
      FROM pod_trans
      WHERE trnCreated
      GROUP BY DATE_FORMAT(trnCreated,'%d %b %Y')");
    $result = $hasil->result_array();
        return $result;
  }

	function get_chart(){
		$query=$this->db->query("select * from pod_trans;");
		return $query;
	}
	

}