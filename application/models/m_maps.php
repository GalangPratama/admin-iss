<?php
class M_maps extends CI_Model{
	private $table="pod_transdata";
	private $primary="trnHawb";
	function semua($limit,$offset){
		$this->db->limit($limit,$offset);
		return $this->db->get($this->table);
	}

	function getAll(){
		$query = $this->db->query('SELECT pod_transdata.*, pod_tmp.trnDeliveredByName FROM pod_transdata left JOIN pod_tmp ON pod_tmp.trnNoHAWB=pod_transdata.trnHawb WHERE DATE_FORMAT(trnCreated, "%Y-%m-%d") = CURDATE() AND trnCreated IN (SELECT MAX(trnCreated) FROM pod_transdata GROUP BY trnNoPlat)');
		return $query;	
	}

	function getAllBMW(){
		$query = $this->db->query('SELECT pod_transdata.*, pod_tmp.trnDeliveredByName FROM pod_transdata left JOIN pod_tmp ON pod_tmp.trnNoHAWB=pod_transdata.trnHawb WHERE DATE_FORMAT(trnCreated, "%Y-%m-%d") = CURDATE() AND trnAcc = "3691100167" AND trnCreated IN (SELECT MAX(trnCreated) FROM pod_transdata GROUP BY trnNoPlat)');
		return $query;	
	}

	function getAllISS(){
		$query = $this->db->query('SELECT pod_transdata.*, pod_tmp.trnDeliveredByName FROM pod_transdata left JOIN pod_tmp ON pod_tmp.trnNoHAWB=pod_transdata.trnHawb WHERE DATE_FORMAT(trnCreated, "%Y-%m-%d") = CURDATE() AND trnAcc = "3692000010" AND trnCreated IN (SELECT MAX(trnCreated) FROM pod_transdata GROUP BY trnNoPlat)');
		return $query;	
	}

	function cekMap($id){
		$this->db->select('*');
        $this->db->from('pod_tmp');
        $this->db->where('trnHawb',$id);
		$this->db->join('pod_transdata','pod_tmp.trnNoHAWB=pod_transdata.trnHawb', 'left');
		$this->db->group_by('trnNoPlat');
        return $this->db->get();
    }

}