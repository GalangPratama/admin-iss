<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Datakurier extends CI_Model {
  var $table = "pod_tmp";  
  // var $select_column = array("trnHawb", "trnDeliveredBy", "trnNoPlat", "trnReceiver", "trnRelation", "trnNote", "trnStatus", "trnCreated");  
  // var $order_column = array("trnHawb", "trnDeliveredBy", "trnNoPlat", "trnReceiver", "trnRelation", "trnNote", "trnStatus", "trnCreated", null); 

  public function getCountKurier()
  {
      return $this->db->count_all_results('pod_trans', FALSE);
  }

  public function getKurier()
  {
      return $this->db->query('SELECT * FROM pod_trans WHERE trnStatus <> "OPEN"');
  }
   function cekDo(){
        $this->db->select('*');
        $this->db->from('pod_trans');
        $this->db->where('trnStatus <> "OPEN"');
        $this->db->join('tb_assets_pod','pod_trans.trnHawb=tb_assets_pod.trnHawb', 'left');
        return $this->db->get();
    }

  function make_query()  
      {  
        $this->db->select('pod_tmp.*,pod_transdata.trnNoPlat, pod_transdata.trnReceiverRelation, pod_transdata.trnNote', 'pod_transdata.trnCreated');
        $this->db->from('pod_tmp');
        $this->db->join('pod_transdata','pod_tmp.trnNoHAWB=pod_transdata.trnHawb', 'left');
      }  

      function make_iss_delivered(){  
          $this->make_query();
          $this->db->order_By('trnDeliveredDate', "DESC" );
          $this->db->where('trnstatusId = "11"');
          $this->db->where('trnAcc = "3692000010"');
           $query = $this->db->get();  
           return $query->result();  
      }

      function make_iss_return(){  
        $this->make_query();
        $this->db->order_By('trnDeliveredDate', "DESC" );
        $this->db->where('trnstatusId <> "11"');
        $this->db->where('trnAcc = "3692000010"');
         $query = $this->db->get();  
         return $query->result();  
      }

      function make_bmw_delivered(){  
        $this->make_query();
        $this->db->order_By('trnDeliveredDate', "DESC" );
        $this->db->where('trnstatusId = "11"');
        $this->db->where('trnAcc = "3691100167"');
         $query = $this->db->get();  
         return $query->result();  
    }

    function make_bmw_return(){  
      $this->make_query();
      $this->db->order_By('trnDeliveredDate', "DESC" );
      $this->db->where('trnstatusId <> "11"');
      $this->db->where('trnAcc = "3691100167"');
       $query = $this->db->get();  
       return $query->result();  
    }

      function get_all_data()  
      {  
        $this->make_query();  
        $query = $this->db->get();  
        return $query->num_rows();    
      }

      function fetch_single_user($user_id)  
      {  
      $this->db->where("pod_tmp.trnNoHAWB", $user_id);
      $this->db->join('pod_transdata','pod_tmp.trnNoHAWB=pod_transdata.trnHawb', 'left'); 
      $query=$this->db->get('pod_tmp');
      return $query->result(); 
      }  

      function update_crud($user_id, $data)  
      {  
           $this->db->where("trnHawb", $user_id);  
           $this->db->update("pod_trans", $data);  
      } 
      


      // function make_query_index()  
      // {  
      //   $this->db->select('pod_trans.*, pod_assets.astPhoto');
      //   $this->db->from('pod_trans');
      //   $this->db->join('pod_assets','pod_trans.trnHawb=pod_assets.trnHawb', 'left');
      // } 
      
      
      
      // function make_datatables_index(){  
      //     $this->make_query_index();
      //     $this->db->order_By('trnCreated', "DESC" );
      //     $this->db->where('trnCstAcc = "3692000010"');
      //      $query = $this->db->get();  
      //      return $query->result();  
      // }
      // function fetch_single_index($kurier_id)  
      // {  
      //   $this->db->select('pod_trans.trnHawb, pod_trans.trnDeliveredBy, pod_trans.trnNoPlat, pod_trans.trnCstAcc, pod_trans.trnCstName, pod_trans.trnReceiver, pod_trans.trnRelation, pod_trans.trnNote, pod_trans.trnStatus, pod_trans.trnCreated, pod_assets.astSignature, pod_assets.astPhoto');
      //   $this->db->where("pod_trans.trnHawb", $kurier_id);
      //   $this->db->join('pod_assets','pod_trans.trnHawb = pod_assets.trnHawb', 'left'); 
      //   $query = $this->db->get('pod_trans');  
      //   return $query->result();  

      // }
      // function get_all_data_index()  
      // {  
      //   $this->make_query_index();  
      //   $query = $this->db->get();  
      //   return $query->num_rows();    
      // }

}