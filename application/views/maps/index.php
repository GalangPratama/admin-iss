<div class="container">
    <?php
                    echo $map['js'];
                    echo $map['html'];
                ?>
    <?php
                  if ($name == true) {
                    echo "<h3 style='text-align: center; font-weight: bold; margin: 10px 0;'>DAFTAR KURIER</h3>";
                  } else {
                    echo "";
                  }
                  
                ?>

    <div class="row staff-grid-row">
        <?php foreach ($name as $row) { ?>
        <div class="col-md-4 col-sm-6 col-12 col-lg-4 col-xl-3">
            <div class="profile-widget">
                <div class="profile-img">
                    <a href="<?php echo base_url('dashboard/perkurir/'. $row->trnHawb); ?>"><img class="avatar"
                            src="<?php echo base_url('assets/img/operator1.png');?>" alt=""></a>
                </div>
                <h4 class="user-name m-t-10 m-b-0 text-ellipsis"><a
                        href="<?php echo base_url('dashboard/perkurir/'. $row->trnHawb); ?>">
                        <?php echo $row->trnDeliveredByName ?></a></h4>
                <div class="small text-muted"><?php echo $row->trnHawb ?></div>
            </div>
        </div>
        <?php
                    } 
                    ?>
    </div>

</div>