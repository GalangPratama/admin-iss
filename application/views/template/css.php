<!DOCTYPE html>
<html>


<!-- Mirrored from dreamguys.co.in/preadmin/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Jul 2018 03:28:52 GMT -->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/favicon1.png'); ?>">
    <title>RPOD - <?php echo $title ?> </title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" type="text/css"
        href="<?php echo base_url('assets/bootstrap-4.3.1/dist/css/bootstrap.min.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/templates/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/templates/css/style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/cssku.css'); ?>">

    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-94034622-3');
    </script>

    <!-- Dynamic Stylesheet -->
    <?php if(isset($includes_for_layout['css']) AND count($includes_for_layout['css']) > 0): ?>
    <?php foreach($includes_for_layout['css'] as $css): ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $css['file']; ?>"
        <?php echo ($css['options'] === NULL ? '' : ' media="' . $css['options'] . '"'); ?>>
    <?php endforeach; ?>
    <?php endif; ?>


</head>