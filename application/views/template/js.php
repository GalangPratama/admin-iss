    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/templates/js/popper.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/bootstrap-4.3.1/dist/js/bootstrap.min.js');?>">
    </script>
    <script type="text/javascript"
        src="<?php echo base_url('assets/bootstrap-4.3.1/dist/js/bootstrap.bundle.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/templates/js/jquery.slimscroll.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/templates/js/select2.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/templates/js/moment.min.js'); ?>"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/templates/plugins/morris/morris.min.js'); ?>">
    </script>
    <script type="text/javascript" src="<?php echo base_url('assets/templates/plugins/raphael/raphael-min.js'); ?>">
    </script>

    <script type="text/javascript" src="<?php echo base_url('assets/templates/js/app.js'); ?>"></script>
    <script>
$(function() {
    $(".hapus").click(function() {
        var kode = $(this).attr("kode");
        $("#idhapus").val(kode);
        $("#myModal").modal("show");
    });

    $("#konfirmasi").click(function() {
        var kode = $("#idhapus").val();

        $.ajax({
            url: "<?php echo site_url('user/hapus');?>",
            type: "POST",
            data: "kode=" + kode,
            cache: false,
            success: function(html) {
                location.reload();
            }
        })
    })
});
    </script>
    <!-- Dynamic JQuery -->
    <?php if(isset($includes_for_layout['js']) AND count($includes_for_layout['js']) > 0): ?>
    <?php foreach($includes_for_layout['js'] as $js): ?>
    <?php if($js['options'] === NULL OR $js['options'] == 'footer'): ?>
    <script type="text/javascript" src="<?php echo $js['file']; ?>"></script>
    <?php endif; ?>
    <?php endforeach; ?>
    <?php endif; ?>