<?php $this->load->view('template/css'); ?>

<body>
    <div class="main-wrapper">
        <!-- Include Header -->
        <?php $this->load->view('template/header'); ?>
        <!-- Include Sidebar -->
        <?php $this->load->view('template/sidebar'); ?>

        <div class="page-wrapper">
            <?php echo $content;?>
        </div>

        <!-- Include Footer -->
        <?php $this->load->view('template/footer'); ?>
    </div>
    <div class="sidebar-overlay" data-reff=""></div>
    <?php $this->load->view('template/js'); ?>

</body>
<!-- Modal Delete -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="idhapus" id="idhapus">
                <input type="hidden" name="idhapus2" id="idhapus2">
                <p>Apakah anda yakin ingin menghapus data ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id="konfirmasi">Hapus</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Modal Delete -->

<!-- Modal Image Laporan -->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-md-6">
                    <h4>No. AWB &nbsp;: <span id="trnNoHAWB"></span> </h4>
                </div>
                <div class="col-md-6">
                    <h4>Date &nbsp;: <span id="trnDate"></span>&nbsp;<span id="trnTime"></span> </h4>
                </div>
            </div>
            <div class="modal-body">
                <div class="detail-head">
                    <h4 class="text-center">Detail Laporan</h4>
                </div>
                <div class="row" id="row-ModalContent">
                    <div class="col-md-2 label-left">
                        <h5>Kurier Name</h5>
                        <h5>No. Plat</h5>
                        <h5>POD Date</h5>
                        <h5>Customer ID</h5>
                        <h5>Customer</h5>
                    </div>
                    <div class="col-md-4 label-right">
                        <h5>: <span id="trnDeliveredByName"></span></h5>
                        <h5>: <span id="trnNoPlat"></span></h5>
                        <h5>: <span id="trnDeliveredDate"></span> &nbsp; <span id="trnDeliveredTimePOD"></span> </h5>
                        <h5>: <span id="trnAcc"></span></h5>
                        <h5>: <span id="trnName"></span></h5>
                    </div>
                    <div class="col-md-2 label-left">
                        <h5>JIT</h5>
                        <h5>Name Area</h5>
                        <h5>Alamat</h5>
                        <h5>No. Telephone</h5>
                        <h5>PIC</h5>
                        <h5>Colly</h5>
                        <h5>Weight</h5>
                        <h5>Receipt Name</h5>
                        <h5>NIK/Position Receipt</h5>
                        <h5>Status</h5>
                    </div>
                    <div class="col-md-4 label-right">
                        <h5>: <span id="trnSpecialInstruction"></span></h5>
                        <h5>: <span id="trnConsName"></span></h5>
                        <h5>: <span id="trnConsAlm1" style="font-size: 12px;"></span></h5>
                        <h5>: <span id="trnPhone"></span></h5>
                        <h5>: <span id="trnConsContact"></span></h5>
                        <h5>: <span id="trnkoli"></span></h5>
                        <h5>: <span id="trnWeight"></span></h5>
                        <h5>: <span id="trnReceiver"></span></h5>
                        <h5>: <span id="trnReceiverRelation"></span></h5>
                        <h5>: <span id="trnReceivernotes"></span></h5>
                    </div>
                </div>

                <div class="Image">
                    <div class="judul-img">
                        <h4>IMAGE</h4>
                    </div>
                    <div class="img">
                        <span id="jdimg1"> </span>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Image Laporan -->

<!-- Modal Image Laporan -->
<div class="modal fade" id="tesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <div class="left-mdl">
                        Transaction ID : <span id="trnHawb"></span>
                    </div>
                    <div class="right-mdl">
                        Date <span style="margin-left: 60px;"> : <span id="trnCreated"></span></span>
                    </div>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="mdl_gambar" class="table table-striped">
                    <thead style="width: 100%;">
                        <tr>
                            <th class="text-center">Transaction ID</th>
                            <th class="text-center">Kurier Name</th>
                            <th class="text-center">No. Plat</th>
                            <th class="text-center">Customer ID</th>
                            <th class="text-center">Customer</th>
                            <th class="text-center">Receiver</th>
                            <th class="text-center">NIK</th>
                            <th class="text-center">Note</th>
                            <th class="text-center">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center" id="idtrnHawb"> </td>
                            <td class="text-center" id="trnDeliveredBy"> </td>
                            <td class="text-center" id="trnNoPlat"> </td>
                            <td class="text-center" id="trnCstAcc"> </td>
                            <td class="text-center" id="trnCstName"> </td>
                            <td class="text-center" id="trnReceiver"> </td>
                            <td class="text-center" id="trnRelation"> </td>
                            <td class="text-center" id="trnNote"> </td>
                            <td class="text-center" id="trnStatus"> </td>
                        </tr>
                    </tbody>
                </table>
                <div class="Image">
                    <div class="judul-img">
                        <h4>IMAGE</h4>
                    </div>
                    <div class="img">
                        <span id="jdimg1"> </span>
                        <span id="jdimg2"> </span>
                        <span id="jdimg3"> </span>
                        <span id="jdimg4"> </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Image Laporan -->

<!-- Modal Add Vehicle -->
<div class="modal fade" id="add_vehicle" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Tambah Kendaraan</h5>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'vehicle/tambah'?>">
                <div class="modal-body">

                    <div class="form-group">
                        <label>Kendaraan</label>
                        <select class="form-control selectric" name="type" required>
                            <option>--Pilih Kendaraan--</option>
                            <option value="motor">Motor</option>
                            <option value="mobil">Mobil</option>
                        </select>
                        <?php echo form_error('type');?>
                    </div>

                    <div class="form-group">
                        <label>Plat Nomer</label>
                        <div class="input-group">
                            <input type="text" name="plat" class="form-control" required>
                        </div>
                        <?php echo form_error('plat');?>
                    </div>

                    <div class="form-group">
                        <div class="control-label">Status Aktif</div>
                        <div class="custom-switches-stacked mt-2">
                            <label class="custom-switch">
                                <input type="radio" name="status" value="Y" class="custom-switch-input" checked>
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">Aktif</span>
                            </label>
                            <label class="custom-switch">
                                <input type="radio" name="status" value="N" class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">Non Aktif</span>
                            </label>
                        </div>
                        <?php echo form_error('status');?>
                    </div>

                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <button class="btn btn-info">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Add Vehicle -->

<!-- Modal Add Vehicle -->
<div class="modal fade" id="add_produk" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Tambah Produk</h5>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'produk/tambah'?>">
                <div class="modal-body">

                    <div class="form-group">
                        <label>Nama Produk</label>
                        <div class="input-group">
                            <input type="text" name="nama_produk" class="form-control" required>
                        </div>
                        <?php echo form_error('nama_produk');?>
                    </div>

                    <div class="form-group">
                        <div class="control-label">Status Aktif</div>
                        <div class="custom-switches-stacked mt-2">
                            <label class="custom-switch">
                                <input type="radio" name="status" value="Y" class="custom-switch-input" checked>
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">Aktif</span>
                            </label>
                            <label class="custom-switch">
                                <input type="radio" name="status" value="N" class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">Non Aktif</span>
                            </label>
                        </div>
                        <?php echo form_error('status');?>
                    </div>

                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <button class="btn btn-info">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Add Vehicle -->

<!-- Mirrored from dreamguys.co.in/preadmin/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Jul 2018 03:29:13 GMT -->

</html>