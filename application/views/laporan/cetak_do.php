<html>
	<head>
		<title><?php echo $title;?></title>
	
	<link href="<?php echo base_url('assets/css/pdf.css');?>" rel="stylesheet" type="text/css"/>
	</head>
	<body onload="window.print();">
		<?php foreach($job as $data) { ?>    
    <h4 style="text-align:center;">
        DELIVERED STATUS
    </h4>

    <hr>
    <table class="table table-bordered">
        <thead style="background:lightgray;">
            <tr>
                <td>No Surat Jalan</td>
                <td>Nama Barang</td>
                <td>QTY</td>
                
            </tr>
        </thead>
            <tr>
                <td><?php echo $data->jdNoDelivery;?></td>
                <td><?php echo $data->type_of_goods;?></td>
                <td><?php echo $data->jdQty;?></td>
            </tr>
    </table>
    <hr>    

    <div class="row invoice-info">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 invoice-bottom">
            <h5>NOTE</h5>
            <h3>ALAMAT</h3>
            <h5>Address</h5>
            <h5>Date</h5>
            <h5>Courier</h5>
            <h5>Plat Nomer</h5>
            <h5>Area</h5>
            <h5>Recepient</h5>
            <h5>Position</h5>
            <h3>PROVE OF DELIVERY</h3>
            <h5>Submmited By :</h5>
            <br>
            <hr>
            <h5>Signature and Photo : </h5>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 invoice-data" id="invoice-data">
            <p> : &nbsp <?php echo $data->jdNote;?></p>
            <br>
            <p> : &nbsp <?php echo $data->customerAddress;?></p>
            <p> : &nbsp <?php echo date('Y-m-d H:i', strtotime($data->jdCreated)) ;?> WIB</p>
            <p> : &nbsp <?php echo $data->jobCourier;?></p>
            <p> : &nbsp <?php echo $data->plat;?></p>
            <p> : &nbsp <?php echo $data->jobArea;?></p>
            <p> : &nbsp <?php echo $data->jdReceiver;?></p>
            <p> : &nbsp <?php echo $data->jdReceiverTitle;?></p>
        </div>
        <div class="col-xs-12 image">
            <?php
             if ($photo[0]->pod_photo != '') { ?>
             <div class="ttd">
                <img width="200" height="160" src="data:image/jpeg;base64, <?php echo $photo[0]->pod_signature; ?>"/>
                <h5>( <?php echo $data->jdReceiver;?> )</h5>
            </div>
                <img width="200" height="160" src="http://api.pda.co.id/api/woi/uploads/<?php echo $photo[0]->pod_photo; ?>"/>
                <img width="200" height="160" src="http://api.pda.co.id/api/woi/uploads/<?php echo $photo[1]->pod_photo; ?>"/>
                <img width="200" height="160" src="http://api.pda.co.id/api/woi/uploads/<?php echo $photo[2]->pod_photo; ?>"/>
           <?php } else { ?>
               <div class="ttd">
                <img width="200" height="160" src="<?php echo base_url('assets/img/no-img.jpg'); ?>"/>
                </div>
         <?php  }
            };       
        ?>
        </div>
            
    </div>

    <br>	

	</body>
</html>