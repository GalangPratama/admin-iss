<!DOCTYPE html>
<html>


<!-- Mirrored from dreamguys.co.in/preadmin/light/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Jul 2018 03:29:19 GMT -->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/favicon1.png'); ?>">
    <title><?php echo $title; ?></title>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/templates/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/templates/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/Login.css'); ?>">
    <!--[if lt IE 9]>
		<script src="assets/templates/js/html5shiv.min.js"></script>
		<script src="assets/templates/js/respond.min.js"></script>
	<![endif]-->
</head>

<body id="login">
    <div class="main-wrapper">
        <div class="account-page">
            <div class="container">
                <div class="account-box">
                    <div class="account-wrapper">
                        <div class="account-logo">
                            <h2>LOGIN ADMIN</h2>
                        </div>
                        <form action="<?php echo base_url('login/auth'); ?>" method="POST">
                            <?php echo validation_errors();?>
                            <?php echo $message;?>
                            <div class="form-group form-focus">
                                <input class="form-control floating" type="text" name="userid" placeholder="Username"
                                    required autofocus>
                            </div>
                            <div class="form-group form-focus">
                                <input class="form-control floating" type="password" name="password"
                                    placeholder="Password" required autofocus>
                            </div>

                            <div class="form-group text-center">
                                <button class="btn btn-primary btn-block account-btn" type="submit">LOGIN</button>
                            </div>
                            <div class="text-center">
                                <a href="#" style="color: #fff;">Forgot your password?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url('assets/templates/js/jquery-3.2.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/templates/js/popper.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/templates/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/templates/js/app.js'); ?>"></script>
</body>


<!-- Mirrored from dreamguys.co.in/preadmin/light/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Jul 2018 03:29:20 GMT -->

</html>