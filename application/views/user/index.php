<div class="section-header">
    <h1 style="text-align: center; margin: 20px 0"><?php echo $title; ?></h1>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active"> <a href="<?php echo site_url('dashboard'); ?>">Dashboard</a></li>
            <li class="breadcrumb-item" aria-current="page"><?php echo $title; ?></li>
        </ol>
    </nav>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="pull pull-right">
            <a href="<?php echo site_url('user/tambah');?>" class="btn btn-primary"><i
                    class="glyphicon glyphicon-plus"></i> Tambah User</a>
        </div>
    </div>
</div>

<table class="table table-striped">
    <thead>
        <tr style="text-align: center">
            <th>#</th>
            <th>Nama Lengkap</th>
            <th>Username</th>
            <th>Level</th>
            <th>Foto</th>
            <th colspan="2"></th>
        </tr>
    </thead>
    <?php $no=0; foreach($user as $user): $no++;?>
    <tr>
        <td><?php echo $no;?></td>
        <td><?php echo $user->nama_lengkap;?></td>
        <td><?php echo $user->username;?></td>
        <td><?php  if ($user->level == 1) {
           echo "Super Admin";
        } else if($user->level == 2) {
            echo "Admin ISS";
        }else{
            echo "Admin BMW";
        } ?></td>
        <td><?php echo $user->foto;?></td>
        <td><a href="<?php echo site_url('user/edit/'.$user->username);?>"><i class="fa fa-edit"></i></a></td>
        <td><a href="#" title="Hapus User" class="hapus" kode="<?php echo $user->username;?>"><i class="fa fa-trash"
                    aria-hidden="true"></i></a></td>
    </tr>
    <?php endforeach;?>
</table>