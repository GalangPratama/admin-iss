<div class="section-header">
    <h1 style="text-align: center; margin: 20px 0"><?php echo $title; ?></h1>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active"> <a href="<?php echo site_url('dashboard'); ?>">Dashboard</a></li>
            <li class="breadcrumb-item active"> <a href="<?php echo site_url('user'); ?>">Data User</a></li>
            <li class="breadcrumb-item" aria-current="page"><?php echo $title; ?></li>
        </ol>
    </nav>
</div>

<table class="table">
    <thead>
        <tr style="text-align: center">
            <th>Nama Lengkap</th>
            <th>Username</th>
            <th>Level</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo $user['nama_lengkap'];?></td>
            <td><?php echo $user['username'];?></td>
            <td><?php  if ($user['level'] == 1) {
           echo "Super Admin";
        } else if($user['level'] == 2) {
            echo "Admin ISS";
        }else{
            echo "Admin BMW";
        } ?></td>
        </tr>
    </tbody>
</table>

<hr>
<div class="well">
    <a href="<?php echo site_url('user');?>" class="btn btn-primary">Kembali</a>
</div>