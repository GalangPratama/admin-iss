<div class="row">
	<div class="col-md-8">
		<form class="form-horizontal" action="<?php echo site_url('user/filter');?>" method="post">
			<div class="form-group">
				<label class="col-lg-4 control-label">Username / Nama User</label>
				<div class="col-lg-5">
					<input type="text" class="form-control">
				</div>
				<div class="col-lg-3">
					<button class="btn btn-default"><i class="glyphicon glyphicon-search"></i> Cari</button>
				</div>
			</div>
		</form>
	</div>


	<div class="col-md-4">
		<div class="pull pull-right">
			<a href="<?php echo site_url('user/tambah');?>" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah User</a>
		</div>
	</div>
</div>

<table class="table table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Nama Lengkap</th>
			<th>Username</th>
			<th>Level</th>
			<th></th>
		</tr>
	</thead>
	<?php $no=0; foreach($user as $user): $no++;?>
	<tr>
		<td><?php echo $no;?></td>
		<td><?php echo $user->nama_lengkap;?></td>
		<td><?php echo $user->username;?></td>
		<td><?php echo $user->level;?></td>
		<td><a href="#" title="Hapus User" class="hapus" kode="<?php echo $user->username;?>"><i class="glyphicon glyphicon-trash"></i></a></td>
	</tr>
	<?php endforeach;?>
</table>

<script>
	$(function(){
		$(".hapus").click(function(){
			var kode=$(this).attr("kode");
			$("#idhapus").val(kode);
			$("#myModal").modal("show");
		});

		$("#konfirmasi").click(function(){
			var kode=$("#idhapus").val();

			$.ajax({
				url:"<?php echo site_url('user/hapus');?>",
				type:"POST",
				data:"kode="+kode,
				cache:false,
				success:function(html){
					location.reload();
				}
			})
		})
	});
</script>