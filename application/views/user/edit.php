<div class="section-header">
    <h1 style="text-align: center; margin: 20px 0"><?php echo $title; ?></h1>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active"> <a href="<?php echo site_url('dashboard'); ?>">Dashboard</a></li>
            <li class="breadcrumb-item active"> <a href="<?php echo site_url('user'); ?>">Data User</a></li>
            <li class="breadcrumb-item" aria-current="page"><?php echo $title; ?></li>
        </ol>
    </nav>
</div>
<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
    <?php echo $message;?>
    <div class="form-group">
        <label class="col-lg-2 control-label">Username</label>
        <div class="col-lg-4">
            <input type="text" readonly="readonly" value="<?php echo $user['username'];?>" class="form-control"
                name="user">
        </div>
        <?php echo form_error('user');?>
    </div>


    <div class="form-group">
        <label class="col-lg-2 control-label">Nama Lengkap</label>
        <div class="col-lg-4">
            <input type="text" value="<?php echo $user['nama_lengkap'];?>" class="form-control" name="nama">
        </div>
        <?php echo form_error('nama');?>
    </div>

    <div class="form-group">
        <label class="col-lg-2 control-label">Level</label>
        <div class="col-lg-4">
            <select name="level" class="form-control">
                <option value="<?php echo $user['level'];?>"><?php  if ($user['level'] == 1) {
                echo 'Super Admin';
                } else if($user['level'] == 2) {
                    echo 'Admin ISS';
                }else{
                    echo 'Admin BMW';
                } ?></option>
                <option value="1">Super Admin</option>
                <option value="2">Admin ISS</option>
                <option value="3">Admin BMW</option>
            </select>
        </div>
        <?php echo form_error('level');?>
    </div>

    <div class="form-group">
        <label class="col-lg-2 control-label">Foto</label>
        <div class="col-lg-4">
            <img src="<?php echo base_url('assets/img/'.$user['foto']);?>" width="140px" height="140px"
                class="img img-rounded">
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-2 control-label"></label>
        <div class="col-lg-4">
            <input type="file" name="gambar">
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-2 control-label"></label>
        <div class="col-lg-4">
            <button class="btn btn-primary"><i class="glyphicon glyphicon-saved"></i> Simpan</button>
            <a href="<?php echo site_url('user');?>" class="btn btn-default">Kembali</a>
        </div>
    </div>
</form>