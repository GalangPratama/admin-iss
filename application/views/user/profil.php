<div class="section-header">
    <h1 style="text-align: center; margin: 20px 0"><?php echo $title; ?></h1>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active"> <a href="<?php echo site_url('dashboard'); ?>">Dashboard</a></li>
            <li class="breadcrumb-item active"> <a href="<?php echo site_url('user'); ?>">Data User</a></li>
            <li class="breadcrumb-item" aria-current="page"><?php echo $title; ?></li>
        </ol>
    </nav>
</div>
<div class="form-group row">
    <label class="col-md-4 control-label align-self-center" style="text-align: center">Username</label>
    <div class="col-md-4">
        <input type="text" readonly="readonly" value="<?php echo $user['username'];?>" class="form-control" name="user">
    </div>
    <?php echo form_error('user');?>
</div>


<div class="form-group row">
    <label class="col-md-4 control-label align-self-center" style="text-align: center">Nama Lengkap</label>
    <div class="col-md-4">
        <input type="text" readonly="readonly" value="<?php echo $user['nama_lengkap'];?>" class="form-control"
            name="nama">
    </div>
    <?php echo form_error('nama');?>
</div>

<div class="form-group row">
    <label class="col-md-4 control-label align-self-center" style="text-align: center">Level</label>
    <div class="col-md-4">
        <input type="text" readonly="readonly" value="<?php 
        if ($user['level'] == 1) {
           echo "Super Admin";
        } else if($user['level'] == 2) {
            echo "Admin";
        }else{
            echo "Vendor";
        }?>
        " class="form-control" name="nama">
    </div>
    <?php echo form_error('level');?>
</div>

<div class="form-group row">
    <label class="col-md-4 control-label align-self-center" style="text-align: center">Foto</label>
    <div class="col-md-4">
        <img src="<?php echo base_url('assets/img/'.$user['foto']);?>" width="140px" height="140px"
            class="img img-rounded">
    </div>
</div>