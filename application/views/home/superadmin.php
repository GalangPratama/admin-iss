<div class="content container-fluid">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-lg-6 col-xl-6">
            <div class="dash-widget dash-widget5">
                <span class="dash-widget-icon bg-success"> D </i></span>
                <div class="dash-widget-info">
                    <h3><?php echo $deliviss; ?></h3>
                    <span>POD PROJECT BMW</span>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-6 col-xl-6">
            <div class="dash-widget dash-widget5">
                <span class="dash-widget-icon bg-danger"> R </span>
                <div class="dash-widget-info">
                    <h3><?php echo $returniss; ?></h3>
                    <span>RETURN PROJECT ISS</span>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-6 col-xl-6">
            <div class="dash-widget dash-widget5">
                <span class="dash-widget-icon bg-success"> D </i></span>
                <div class="dash-widget-info">
                    <h3><?php echo $delivbmw; ?></h3>
                    <span>POD PROJECT BMW</span>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-6 col-xl-6">
            <div class="dash-widget dash-widget5">
                <span class="dash-widget-icon bg-danger"> R </span>
                <div class="dash-widget-info">
                    <h3><?php echo $returnbmw; ?></h3>
                    <span>RETURN PROJECT BMW</span>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
            <figure class="highcharts-figure">
                <div id="container"></div>
            </figure>
        </div>
    </div>
</div>